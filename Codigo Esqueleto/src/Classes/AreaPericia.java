package Classes;

public class AreaPericia {
    
    private String areapericia;

    public AreaPericia(String areapericia) {
        this.areapericia = areapericia;
    }

    public AreaPericia() {
    }

    public String getAreapericia() {
        return areapericia;
    }

    public void setAreapericia(String areapericia) {
        this.areapericia = areapericia;
    }
    
    
    
}
