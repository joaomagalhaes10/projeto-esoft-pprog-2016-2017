package Classes;

import java.util.List;

public class ListaOrganizadores {

    List<Organizador> ListaOrganizadores;

    /**
     *
     * @param Utilizador
     */
    public void addOrganizador(Utilizador Utilizador) {
        Organizador Organizador = new Organizador();
        Organizador.setUtilizador(Utilizador);
        validaOrganizador(Organizador);
        add(Organizador);
    }

    /**
     *
     * @param Organizador
     * @return 
     */
    public boolean validaOrganizador(Organizador Organizador) {
        return !ListaOrganizadores.contains(Organizador);
    }

    /**
     *
     * @param Organizador
     */
    public void add(Organizador Organizador) {
        if (validaOrganizador(Organizador)){
            ListaOrganizadores.add(Organizador);
        }
    }
    
    public boolean contemOrganizador(Utilizador Utilizador){
        Organizador Organizador = new Organizador();
        Organizador.setUtilizador(Utilizador);
        
        return ListaOrganizadores.contains(Organizador);
    }

}
