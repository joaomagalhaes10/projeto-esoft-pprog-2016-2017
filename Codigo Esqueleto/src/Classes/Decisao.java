package Classes;

public class Decisao {

    private String dec;
    private String txt;

    /**
     *
     * @param dec
     * @param txt
     */
    public Decisao(String dec, String txt) {
        this.dec = dec;
        this.txt = txt;
    }

    public Decisao() {
    }

    /**
     *
     * @return
     */
    public String getDec() {
        return dec;
    }

    /**
     *
     * @return
     */
    public String getTxt() {
        return txt;
    }

    /**
     *
     * @param dec
     */
    public void setDec(String dec) {
        this.dec = dec;
    }

    /**
     *
     * @param txt
     */
    public void setTextoDesc(String txt) {
        this.txt = txt;
    }

    public void valida() {

    }

}
