package Classes;

import java.util.ArrayList;
import java.util.List;

public class RegistoEventos {

    List<Evento> ListaEventos;

    /**
     *
     * @return
     */
    public Evento novoEvento() {
        return new Evento();

    }

    /**
     *
     * @param Evento
     * @return
     */
    public boolean validaEvento(Evento Evento) {

        Evento.valida();

        return ListaEventos.contains(Evento);

    }

    /**
     *
     * @param Evento
     */
    public void registaEvento(Evento Evento) {
        validaEvento(Evento);
        add(Evento);
    }

    /**
     *
     * @param Evento
     */
    public void add(Evento Evento) {
        ListaEventos.add(Evento);
    }

    /**
     *
     * @param email
     * @return
     */
    public List<Evento> getListaEventosOrg(String email) {
        CentroDeEventos CentroDeEventos = new CentroDeEventos();

        List<Evento> EventoOrg = new ArrayList();

        RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();

        Utilizador Utilizador = RegistoUtilizadores.getUtilizador(email);

        for (Evento evento : ListaEventos) {
            ListaOrganizadores ListaOrganizadores = (ListaOrganizadores) evento.getListaOrganizadores();
            boolean b = ListaOrganizadores.contemOrganizador(Utilizador);

            if (b == true) {
                EventoOrg.add(evento);
            }
        }

        return EventoOrg;
    }

    /**
     *
     * @param email
     * @return
     */
    public List<Evento> getListaEventoFAE(String email) {
        CentroDeEventos CentroDeEventos = new CentroDeEventos();

        List<Evento> EventoFAE = new ArrayList();

        RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();

        Utilizador Utilizador = RegistoUtilizadores.getUtilizador(email);

        for (Evento evento : ListaEventos) {
            ListaFAE ListaFAE = (ListaFAE) evento.getListaFAE();
            boolean b = ListaFAE.contemFAE(Utilizador);
            boolean b1 = evento.isEventoEmEstadoCandAtribuida();
            if (b == true && b1 == true) {
                EventoFAE.add(evento);
            }
        }

        return EventoFAE;
    }

    /**
     *
     * @return
     */
    public List<Evento> getListaEventos() {
        return ListaEventos;
    }

    /**
     *
     * @param email
     * @return
     */
    public List<Evento> getListaCongressosOrg(String email) {
        CentroDeEventos CentroDeEventos = new CentroDeEventos();

        List<Evento> CongressosOrg = new ArrayList();

        RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();

        Utilizador Utilizador = RegistoUtilizadores.getUtilizador(email);

        for (Evento evento : ListaEventos) {

            if (evento.getTipo().equalsIgnoreCase("Congresso")) {
                ListaOrganizadores ListaOrganizadores = (ListaOrganizadores) evento.getListaOrganizadores();
                boolean b = ListaOrganizadores.contemOrganizador(Utilizador);

                if (b == true) {
                    CongressosOrg.add(evento);
                }
            }
        }

        return CongressosOrg;
    }

    public List getListaCandidaturasRep(String email) {
        CentroDeEventos CentroDeEventos = new CentroDeEventos();

        List<Candidatura> CandidaturasRep = new ArrayList();

        RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();

        Utilizador Utilizador = RegistoUtilizadores.getUtilizador(email);
        
        for (Evento evento : ListaEventos) {
            
            ListaCandidaturas ListaCandidaturas = (ListaCandidaturas) evento.getListaCandidaturas();
            
            List<Candidatura> CandSub = ListaCandidaturas.getListaCandidaturasSubmissao();
            
            for (Candidatura candidatura: CandSub){
                
                boolean b = ListaCandidaturas.isCandDeRepresentante(Utilizador);
                
                if(b==false){
                    CandSub.remove(candidatura);
                }
            }             
        }
        return CandidaturasRep;
    }

}
