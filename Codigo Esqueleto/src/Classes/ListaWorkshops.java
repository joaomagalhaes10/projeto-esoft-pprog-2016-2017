package Classes;

import java.util.List;

public class ListaWorkshops {

    List<Workshop> ListaWorkshops;

    public List<Workshop> getListaWorkshops() {
        return ListaWorkshops;
    }
    
    /**
     *
     * @return
     */
    public Workshop novoWorkshop() {
        return new Workshop();
    }

    /**
     *
     * @param Workshop
     * @return
     */
    public boolean validaWorkshop(Workshop Workshop) {
        Workshop.valida();

        return !ListaWorkshops.contains(Workshop);
    }

    /**
     *
     * @param Workshop
     */
    public void registaWorkshop(Workshop Workshop) {
        if (validaWorkshop2(Workshop)) {
            add(Workshop);
        }

    }

    /**
     *
     * @param Workshop
     */
    public boolean validaWorkshop2(Workshop Workshop) {
        return !ListaWorkshops.contains(Workshop);
    }

    /**
     *
     * @param Workshop
     */
    public void add(Workshop Workshop) {
        ListaWorkshops.add(Workshop);
    }
    
    public void registaAlteracoes(Workshop Workshop, Workshop Workshop_clone){
        if(validaWorkshop2(Workshop_clone)){
            ListaWorkshops.remove(Workshop);
            ListaWorkshops.add(Workshop_clone);
        }       
    }
    
    public String getInfoWorkshop(Workshop Workshop){
        return Workshop.toString();
    }

}
