package Classes;

import java.util.List;

public class RegistoUtilizadores {

    List<Utilizador> ListaUtilizadoresConf;
    List<Utilizador> ListaUtilizadoresNaoConf;

    /**
     *
     * @return
     */
    public List<Utilizador> getListaUtilizadoresConf() {
        return ListaUtilizadoresConf;
    }

    /**
     *
     * @return
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     *
     * @param Utilizador
     * @return
     */
    public boolean validaUtilizador(Utilizador Utilizador) {
        Utilizador.valida();

        return !ListaUtilizadoresConf.contains(Utilizador);
    }

    /**
     *
     * @param Utilizador
     */
    public void registaUtilizador(Utilizador Utilizador) {
        validaUtilizador2(Utilizador);
        add(Utilizador);
    }

    /**
     *
     * @param Utilizador
     * @return 
     */
    public boolean validaUtilizador2(Utilizador Utilizador) {
        return !ListaUtilizadoresConf.contains(Utilizador);
    }

    /**
     *
     * @param Utilizador
     */
    public void add(Utilizador Utilizador) {
        ListaUtilizadoresNaoConf.add(Utilizador);
    }

    /**
     *
     * @return
     */
    public List<Utilizador> getListaUtilizadoresNaoConf() {
        return ListaUtilizadoresNaoConf;
    }

    /**
     *
     * @param Utilizador
     * @return
     */
    public String getInfoUtilizador(Utilizador Utilizador) {
        return Utilizador.toString();
    }

    /**
     *
     * @param Utilizador
     */
    public void converterParaUtilizadorConf(Utilizador Utilizador) {
        ListaUtilizadoresNaoConf.remove(Utilizador);
        ListaUtilizadoresConf.add(Utilizador);

        validaUtilizador2(Utilizador);
    }

    /**
     *
     * @return
     */
    public List<GestorDeEventos> getListaGestores() {
        // TODO - implement Evento.getListaAtribuicoes
        throw new UnsupportedOperationException();
    }

    public Utilizador getUtilizador(String email) {
        Utilizador util = new Utilizador();

        for (Utilizador Utilizador : ListaUtilizadoresConf) {
            if (Utilizador.getEmail().equals(email)) {
                util = Utilizador;
            }

        }
        return util;
    }
}
