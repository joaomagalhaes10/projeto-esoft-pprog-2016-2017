package Classes;

import java.util.List;

public class ListaGestores {

    List<GestorDeEventos> ListaGestores;

    /**
     *
     * @param Utilizador
     * @return
     */
    public GestorDeEventos novoGestor(Utilizador Utilizador) {
        GestorDeEventos GestorDeEventos = new GestorDeEventos();
        GestorDeEventos.setUtilizador(Utilizador);

        validaGestor(GestorDeEventos);

        return GestorDeEventos;
    }

    /**
     *
     * @param GestorDeEventos
     * @return
     */
    public boolean validaGestor(GestorDeEventos GestorDeEventos) {
        return !ListaGestores.contains(GestorDeEventos);
    }

    /**
     *
     * @param GestorDeEventos
     */
    public void registaGestor(GestorDeEventos GestorDeEventos) {
        if (validaGestor(GestorDeEventos)) {
            add(GestorDeEventos);
        }
    }

    /**
     *
     * @param GestorDeEventos
     */
    public void add(GestorDeEventos GestorDeEventos) {
        ListaGestores.add(GestorDeEventos);
    }

}
