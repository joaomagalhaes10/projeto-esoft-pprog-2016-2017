package Classes;

import Estados.EstadoEvento;

public interface Agendavel {
    
    public abstract void agendamento(EstadoEvento EstadoEvento, String data);
    
}
