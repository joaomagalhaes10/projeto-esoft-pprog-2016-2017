package Classes;

public class Utilizador {

    private String nome;
    private String email;
    private String username;
    private String password;
    private String idioma;
    private String fusohorario;

    /**
     *
     * @param nome
     * @param email
     * @param username
     * @param password
     * @param idioma
     * @param fusohorario
     */
    public Utilizador(String nome, String email, String username, String password, String idioma, String fusohorario) {
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
        this.idioma = idioma;
        this.fusohorario = fusohorario;
    }

    public Utilizador() {
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @return
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     *
     * @return
     */
    public String getFusohorario() {
        return fusohorario;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @param idioma
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     *
     * @param fusohorario
     */
    public void setFusohorario(String fusohorario) {
        this.fusohorario = fusohorario;
    }

    public void valida() {

    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Nome: " + this.nome + "/nEmail: " + this.email + "Username: " + this.username;
    }

}
