package Classes;

import java.util.List;

public class ListaFAE {

    List<FAE> ListaFAE;

    /**
     *
     * @param u
     * @param e
     */
    public void addFAE(Utilizador u, Evento e) {
        FAE fae = new FAE();
        fae.setUtilizador(u);
        validaFAE(fae);
    }

    /**
     *
     * @param fae
     * @return 
     */
    public boolean validaFAE(FAE fae) {
        return !ListaFAE.contains(fae);
    }

    /**
     *
     * @param fae
     */
    public void registaFAE(FAE fae) {
        if(validaFAE(fae)){
            add(fae);
        }        
    }

    /**
     *
     * @param fae
     */
    public void add(FAE fae) {
        ListaFAE.add(fae);
    }

    /**
     *
     * @return
     */
    public List<FAE> getListaFAE() {
        return ListaFAE;
    }
    
    public boolean contemFAE(Utilizador Utilizador){
        FAE Fae = new FAE();
        Fae.setUtilizador(Utilizador);
        
        return ListaFAE.contains(Fae);
    }

}
