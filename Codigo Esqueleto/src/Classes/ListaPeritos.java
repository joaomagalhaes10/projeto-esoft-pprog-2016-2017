package Classes;

import java.util.List;

public class ListaPeritos {
    
    List<Perito> ListaPeritos;

    public List<Perito> getListaPeritos() {
        return ListaPeritos;
    }
    
    public Perito novoPerito(){
        return new Perito();
    }
    
    public boolean validaPerito(Perito Perito){
        return !ListaPeritos.contains(Perito);
    }
    
    public void registaPerito(Perito Perito){
        if(validaPerito(Perito)){
            ListaPeritos.add(Perito);
        }
    }
    
}
