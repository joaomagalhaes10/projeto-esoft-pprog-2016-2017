package Classes;

import java.util.List;

public class ListaTemasWorkshop {
    
    List<Tema> ListaTemasWorkshop;
    
    public void addTema(Tema Tema){
        if(valida(Tema)){
            ListaTemasWorkshop.add(Tema);
        }
    }
    
    public boolean valida(Tema Tema){
        return !ListaTemasWorkshop.contains(Tema);
    }
    
}
