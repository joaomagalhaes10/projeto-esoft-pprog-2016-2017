package Classes;

import Estados.*;
import java.util.List;

public class Evento {

    private String tipo;
    private String titulo;
    private String desc;
    private String local;
    private String datainicio;
    private String datafim;
    private String submissao;

    /**
     *
     * @param tipo
     * @param titulo
     * @param desc
     * @param local
     * @param datainicio
     * @param datafim
     * @param submissao
     */
    public Evento(String tipo, String titulo, String desc, String local, String datainicio, String datafim, String submissao) {
        this.tipo = tipo;
        this.titulo = titulo;
        this.desc = desc;
        this.local = local;
        this.datainicio = datainicio;
        this.datafim = datafim;
        this.submissao = submissao;
    }

    public Evento() {

    }

    /**
     *
     * @return
     */
    public String getTipo() {
        return tipo;
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     *
     * @return
     */
    public String getDesc() {
        return desc;
    }

    /**
     *
     * @return
     */
    public String getLocal() {
        return local;
    }

    /**
     *
     * @return
     */
    public String getDataInicio() {
        return datainicio;
    }

    /**
     *
     * @return
     */
    public String getDataFim() {
        return datafim;
    }

    /**
     *
     * @return
     */
    public String getSubmissao() {
        return submissao;
    }

    /**
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     *
     * @param desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     *
     * @param local
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     *
     * @param datainicio
     */
    public void setDataInicio(String datainicio) {
        this.datainicio = datainicio;
    }

    /**
     *
     * @param datafim
     */
    public void setDataFim(String datafim) {
        this.datafim = datafim;
    }

    /**
     *
     * @param submissao
     */
    public void setSubmissao(String submissao) {
        this.submissao = submissao;
    }

    public List<ListaOrganizadores> getListaOrganizadores() {
        // TODO - implement Evento.valida
        throw new UnsupportedOperationException();
    }

    public void valida() {

    }

    public List<FAE> getListaFAE() {
        // TODO - implement Evento.getListaFAE
        throw new UnsupportedOperationException();
    }

    public List<Candidatura> getListaCandidaturas() {
        // TODO - implement Evento.getListaCandidaturas
        throw new UnsupportedOperationException();
    }

    public List<Atribuicao> getListaAtribuicoes() {
        // TODO - implement Evento.getListaAtribuicoes
        throw new UnsupportedOperationException();
    }

    public List<Workshop> getListaWorkshop() {
        // TODO - implement Evento.getListaWorkshop
        throw new UnsupportedOperationException();
    }

    public void setEstado(EstadoEventoInicial estadoEventoInicial) {
        estadoEventoInicial.SetCriado();
    }

    public void setEstado(EstadoEventoCriado estadoEventoCriado) {
        estadoEventoCriado.SetFAE();
    }

    public void setEstado(EstadoEventoFAEDefinidos estadoEventoFAEDefinidos) {
        estadoEventoFAEDefinidos.SetCandAbertas();
    }

    public void setEstado(EstadoEventoCandAbertas estadoEventoCandAbertas) {
        estadoEventoCandAbertas.SetCandFechadas();
    }

    public void setEstado(EstadoEventoCandFechadas estadoEventoCandFechadas) {
        estadoEventoCandFechadas.SetCandAtribuidas();
    }

    public void setEstado(EstadoEventoCandAtribuidas estadoEventoCandAtribuidas) {
        estadoEventoCandAtribuidas.SetCandAvaliadas();
    }

    public void setEstado(EstadoEventoCandAvaliadas estadoEventoCandAvaliadas) {

    }

    public Atribuicao novaAtribuicao(Candidatura c, FAE fae) {
        return new Atribuicao();
    }

    /**
     *
     * @param Atribuicao
     */
    public void registaAtribuicao(Atribuicao Atribuicao) {

    }
    
    public boolean isEventoEmEstadoCandAtribuida(){
        return true;
    }

}
