package Classes;

import java.util.List;

public class ListaPeritosWorkshop {

    List<Perito> ListaPeritosWorkshop;

    public void addPerito(List<Perito> lps) {
        for (Perito perito : lps) {

            if (valida(perito)) {
                ListaPeritosWorkshop.add(perito);
            }
        }
    }

    public boolean valida(Perito Perito) {
        return !ListaPeritosWorkshop.contains(Perito);
    }
}
