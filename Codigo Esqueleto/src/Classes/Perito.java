package Classes;

public class Perito {
    
    private String nome;

    public Perito(String nome) {
        this.nome = nome;
    }

    public Perito() {
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
    
    
    public void valida(){
        
    }
}
