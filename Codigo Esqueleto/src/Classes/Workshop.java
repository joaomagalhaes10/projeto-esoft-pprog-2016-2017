package Classes;

import java.util.List;

public class Workshop {

    private String codigo;
    private String descricao;
    ListaTemasWorkshop ListaTemasWorkshop;
    ListaPeritosWorkshop ListaPeritosWorkshop;

    public Workshop(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Workshop() {
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void valida() {

    }
    
    public void setTema(Tema Tema){
        ListaTemasWorkshop.addTema(Tema);
        
    }
    
    public void setLista(List<Perito> lps){
        ListaPeritosWorkshop.addPerito(lps);
        
    }
    
    public Workshop criarCloneDeWorkshop(Workshop Workshop){
        return new Workshop();
    }
    
    @Override
    public String toString(){
        return "Código: " + this.codigo + "   Descrição: " + descricao;
    }
}
