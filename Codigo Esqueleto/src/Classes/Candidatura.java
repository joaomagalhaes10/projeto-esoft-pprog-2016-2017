package Classes;

import Estados.*;

public class Candidatura {

    EstadoCandidaturaInicial EstadoCandidaturaInicial;
    EstadoCandidaturaRegistada EstadoCandidaturaRegistada;
    EstadoCandidaturaAtribuida EstadoCandidaturaAtribuida;
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Candidatura:";
    }

    /**
     *
     * @return
     */
    public Decisao novaDecisao() {
        return new Decisao();
    }

    /**
     *
     * @param d
     */
    public void validaDecisao(Decisao d) {
        d.valida();
    }

    /**
     *
     * @param d
     */
    public void validaDecisao2(Decisao d) {

    }

    /**
     *
     * @param d
     */
    public void registaDecisao(Decisao d) {
        validaDecisao2(d);
    }

    public void setDados() {

    }

    public boolean valida() {
        return true;
    }

    public void setEstado(EstadoCandidaturaInicial estadoCandidaturaInicial) {
        EstadoCandidaturaInicial.SetRegistada();
    }
    
    public void setEstado(EstadoCandidaturaRegistada estadoCandidaturaRegistada) {
        EstadoCandidaturaRegistada.SetAtribuida();
    }
    
    public void setEstado(EstadoCandidaturaAtribuida estadoCandidaturaAtribuida) {
        EstadoCandidaturaAtribuida.SetAvaliada();
    }
    
    public boolean isInEstadoSubmetida(){
        return true;
    }
    
    public Candidatura criarCloneCandidaturaRep(Candidatura Candidatura){
        return new Candidatura();
    }
    

}
