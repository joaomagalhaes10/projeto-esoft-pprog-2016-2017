package Classes;

import java.util.ArrayList;
import java.util.List;

public class ListaCandidaturas {

    List<Candidatura> ListaCandidaturas;

    public List<Candidatura> getListaCandidaturas() {
        return ListaCandidaturas;
    }

    /**
     *
     * @param Candidatura
     * @return
     */
    public String getInfoCandidatura(Candidatura Candidatura) {
        String info = Candidatura.toString();

        return info;
    }

    public Candidatura novaCandidatura() {
        return new Candidatura();
    }

    /**
     *
     * @param Candidatura
     * @return
     */
    public boolean validaCandidatura(Candidatura Candidatura) {
        Candidatura.valida();

        if (Candidatura.valida() == true) {
            return !ListaCandidaturas.contains(Candidatura);
        } else {
            return false;
        }
    }

    /**
     *
     * @param Candidatura
     */
    public void registaCandidatura(Candidatura Candidatura) {
        if (validaCandidatura2(Candidatura)) {
            add(Candidatura);
        }
    }

    /**
     *
     * @param Candidatura
     * @return 
     */
    public boolean validaCandidatura2(Candidatura Candidatura) {
        return !ListaCandidaturas.contains(Candidatura);
    }

    /**
     *
     * @param Candidatura
     */
    public void add(Candidatura Candidatura) {
        ListaCandidaturas.add(Candidatura);
    }
    
    public String getInfoCandidaura(Candidatura Candidatura){
        return Candidatura.toString();
    }
    
    public List<Candidatura> getListaCandidaturasSubmissao(){
        List<Candidatura> CandSub = new ArrayList();
        
        for(Candidatura candidatura : ListaCandidaturas){
            
            boolean b = candidatura.isInEstadoSubmetida();
            
            if (b){
                CandSub.add(candidatura);
            }
            
        }
        return CandSub;       
    }
    
    public boolean isCandDeRepresentante(Utilizador Utilizador){
        return true;
    }
    
    public void registaAlteracoes(Candidatura Candidatura, Candidatura Candidatura_clone){
        if(validaCandidatura2(Candidatura_clone)){
            ListaCandidaturas.remove(Candidatura);            
            ListaCandidaturas.add(Candidatura_clone);
        }
    }

}
