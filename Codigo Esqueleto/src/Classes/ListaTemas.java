package Classes;

import java.util.List;

public class ListaTemas {
    
    List<Tema> ListaTemas;

    public List<Tema> getListaTemas() {
        return ListaTemas;
    }
    
    public Tema setNovoTema(){
        return new Tema();
    }
    
    public void registaTema(Tema Tema){
        Workshop Workshop = new Workshop();
        Workshop.ListaTemasWorkshop.addTema(Tema);
        
        if(validaTema(Tema)){
            add(Tema);
        }
        
    }
    
    public boolean validaTema(Tema Tema){
        return !ListaTemas.contains(Tema);
    }
    
    public void add(Tema Tema){
        ListaTemas.add(Tema);
    }
    
    
}
