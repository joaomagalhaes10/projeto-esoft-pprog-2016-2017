package Classes;

public class Tema {
    
    private String nome;
    private String descricao;

    public Tema(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public Tema() {
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Override
    public String toString(){
        return "Nome: " + this.nome + "   Descricão: " + descricao;
    }
    
}
