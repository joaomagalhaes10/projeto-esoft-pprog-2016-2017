package Estados;

import Classes.Evento;

public class EstadoEventoInicial implements EstadoEvento{
    
    Evento Evento;

    public EstadoEventoInicial(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetInicial() {
        return false;
    }

    public boolean SetCriado() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoCriado(Evento));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean valida() {
        return SetInicial()==false;
        
    }
}
