package Estados;

import Classes.Evento;

public class EstadoEventoCriado implements EstadoEvento{

    Evento Evento;

    public EstadoEventoCriado(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetCriado() {
        return false;
    }

    public boolean SetFAE() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoFAEDefinidos(Evento));
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return SetCriado()==false;
        
    }
    
}
