package Estados;

import Classes.Candidatura;

public class EstadoCandidaturaAtribuida implements EstadoCandidatura{


    Candidatura Candidatura;

    public EstadoCandidaturaAtribuida(Candidatura Candidatura) {
        this.Candidatura = Candidatura;
    }

    public boolean SetAtribuida() {
        return false;
    }

    public boolean SetAvaliada() {
        if (valida()) {
            Candidatura.setEstado(new EstadoCandidaturaAtribuida(Candidatura));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean valida() {
        return SetAtribuida() == false;

    }
}
