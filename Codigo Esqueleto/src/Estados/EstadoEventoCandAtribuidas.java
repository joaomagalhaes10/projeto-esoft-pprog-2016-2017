package Estados;

import Classes.Evento;

public class EstadoEventoCandAtribuidas implements EstadoEvento{

       Evento Evento;

    public EstadoEventoCandAtribuidas(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetCandAtribuidas() {
        return false;
    }

    public boolean SetCandAvaliadas() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoCandAvaliadas(Evento));
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return SetCandAtribuidas()==false;
        
    }
}
