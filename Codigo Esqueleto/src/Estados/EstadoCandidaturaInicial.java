package Estados;

import Classes.Candidatura;

public class EstadoCandidaturaInicial implements EstadoCandidatura{
    
    Candidatura Candidatura;

    public EstadoCandidaturaInicial(Candidatura Candidatura) {
        this.Candidatura = Candidatura;
    }

    public boolean SetInicial() {
        return false;
    }

    public boolean SetRegistada() {
        if (valida()) {
            Candidatura.setEstado(new EstadoCandidaturaRegistada(Candidatura));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean valida() {
        return SetInicial()==false;
        
    }
    
}
