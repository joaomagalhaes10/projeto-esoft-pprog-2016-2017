package Estados;

import Classes.Evento;

public class EstadoEventoFAEDefinidos implements EstadoEvento {

    Evento Evento;

    public EstadoEventoFAEDefinidos(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetCriado() {
        return false;
    }

    public boolean SetCandAbertas() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoCandAbertas(Evento));
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return SetCriado()==false;
        
    }
}
