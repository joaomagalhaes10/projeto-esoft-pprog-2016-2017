package Estados;

import Classes.Evento;

public class EstadoEventoCandFechadas implements EstadoEvento{

    Evento Evento;

    public EstadoEventoCandFechadas(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetCandFechadas() {
        return false;
    }

    public boolean SetCandAtribuidas() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoCandAtribuidas(Evento));
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return SetCandFechadas()==false;
        
    }
    
}
