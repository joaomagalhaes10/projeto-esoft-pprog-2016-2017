package Estados;

import Classes.Evento;

public class EstadoEventoCandAbertas implements EstadoEvento{

    Evento Evento;

    public EstadoEventoCandAbertas(Evento Evento) {
        this.Evento = Evento;
    }

    public boolean SetCandAbertas() {
        return false;
    }

    public boolean SetCandFechadas() {
        if (valida()) {
            Evento.setEstado(new EstadoEventoCandFechadas(Evento));
            return true;
        } else {
            return false;
        }
    }

    public boolean valida() {
        return SetCandAbertas()==false;
        
    }
    
}
