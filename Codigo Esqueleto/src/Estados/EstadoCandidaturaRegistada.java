package Estados;

import Classes.Candidatura;

public class EstadoCandidaturaRegistada implements EstadoCandidatura {

    Candidatura Candidatura;

    public EstadoCandidaturaRegistada(Candidatura Candidatura) {
        this.Candidatura = Candidatura;
    }

    public boolean SetRegistada() {
        return false;
    }

    public boolean SetAtribuida() {
        if (valida()) {
            Candidatura.setEstado(new EstadoCandidaturaAtribuida(Candidatura));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean valida() {
        return SetRegistada() == false;

    }
}
