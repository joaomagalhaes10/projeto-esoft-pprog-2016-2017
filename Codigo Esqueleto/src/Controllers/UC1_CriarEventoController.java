package Controllers;

import Classes.CentroDeEventos;
import Classes.Evento;
import Classes.RegistoEventos;
import Classes.RegistoUtilizadores;
import Classes.Utilizador;
import Classes.ListaOrganizadores;
import Estados.EstadoEventoInicial;
import java.util.List;

public class UC1_CriarEventoController {

    CentroDeEventos CentroDeEventos;
    Evento Evento;
    Utilizador Utilizador;
    EstadoEventoInicial EstadoEventoInicial;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();
    ListaOrganizadores ListaOrganizadores = (ListaOrganizadores) Evento.getListaOrganizadores();

    public void novoEvento() {
        this.Evento = RegistoEventos.novoEvento();
    }

    /**
     *
     * @param tipo
     * @param titulo
     * @param desc
     * @param local
     * @param datainicio
     * @param datafim
     * @param submissao
     */
    public void setDados(String tipo, String titulo, String desc, String local, String datainicio, String datafim, String submissao) {
        Evento.setTipo(tipo);
        Evento.setTitulo(titulo);
        Evento.setDesc(desc);
        Evento.setLocal(local);
        Evento.setDataInicio(datainicio);
        Evento.setDataFim(datafim);
        Evento.setSubmissao(submissao);
    }

    public List<RegistoUtilizadores> getListaUtilizadoresConf() {
        List lu = RegistoUtilizadores.getListaUtilizadoresConf();
        return lu;
    }

    public String getInfoUtilizador(Utilizador Utilizador) {
        this.Utilizador = Utilizador;
        String info = RegistoUtilizadores.getInfoUtilizador(Utilizador);
        return info;
    }

    public void addOrganizador() {
        ListaOrganizadores.addOrganizador(Utilizador);
    }

    public void validaEvento() {
        RegistoEventos.validaEvento(Evento);
    }

    public void registaEvento() {
        RegistoEventos.registaEvento(Evento);
        Evento.setEstado(EstadoEventoInicial);
    }

}
