package Controllers;

import Classes.Candidatura;
import Classes.CentroDeEventos;
import Classes.Evento;
import Classes.ListaCandidaturas;
import Classes.RegistoEventos;
import Estados.EstadoCandidaturaInicial;
import java.util.List;

public class UC3_CriarCandidaturaController {

    CentroDeEventos CentroDeEventos;
    Evento Evento;
    Candidatura Candidatura;
    EstadoCandidaturaInicial EstadoCandidaturaInicial;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    ListaCandidaturas ListaCandidaturas = (ListaCandidaturas) Evento.getListaCandidaturas();

    public List<Evento> getListaEventos() {
        List le = RegistoEventos.getListaEventos();
        return le;
    }

    public void setEvento(Evento Evento) {
        this.Evento = Evento;
    }

    public void novaCandidatura() {
        Candidatura = ListaCandidaturas.novaCandidatura();
    }

    public void setDados() {
        Candidatura.setDados();
        ListaCandidaturas.validaCandidatura(Candidatura);
    }

    public void registaCandidatura() {
        ListaCandidaturas.registaCandidatura(Candidatura);
        Candidatura.setEstado(EstadoCandidaturaInicial);
    }

}
