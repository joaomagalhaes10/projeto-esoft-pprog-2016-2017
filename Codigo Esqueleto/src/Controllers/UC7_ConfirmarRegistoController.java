package Controllers;

import Classes.CentroDeEventos;
import Classes.RegistoUtilizadores;
import Classes.Utilizador;
import java.util.List;

public class UC7_ConfirmarRegistoController {

    CentroDeEventos CentroDeEventos;
    Utilizador Utilizador;

    RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();

    public List<Utilizador> getListaUtilizadoresNaoConf() {
        List lunc = RegistoUtilizadores.getListaUtilizadoresNaoConf();
        return lunc;
    }

    public String getInfoUtilizador(Utilizador Utilizador) {
        String info = RegistoUtilizadores.getInfoUtilizador(Utilizador);
        
        return info;
    }

    public void confirmarRegistoUtilizador() {
        RegistoUtilizadores.converterParaUtilizadorConf(Utilizador);
    }

}
