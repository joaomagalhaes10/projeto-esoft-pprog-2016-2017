package Controllers;

import Classes.AlgoritmoDeAtribuicao;
import Classes.Atribuicao;
import Classes.Candidatura;
import Classes.CentroDeEventos;
import Classes.Evento;
import Classes.ListaAtribuicoes;
import Classes.RegistoAlgoritmos;
import Classes.RegistoEventos;
import Estados.EstadoCandidaturaRegistada;
import Estados.EstadoEventoCandAtribuidas;
import java.util.List;

public class UC4_AtribuirCandidaturasController {

    CentroDeEventos CentroDeEventos;
    AlgoritmoDeAtribuicao AlgoritmoDeAtribuicao;
    Evento Evento;
    Atribuicao Atribuicao;
    EstadoEventoCandAtribuidas EstadoEventoCandAtribuidas;
    Candidatura Candidatura;
    EstadoCandidaturaRegistada EstadoCandidaturaRegistada;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    RegistoAlgoritmos RegistoAlgoritmos = CentroDeEventos.getRegistoAlgoritmos();
    ListaAtribuicoes ListaAtribuicoes = (ListaAtribuicoes) Evento.getListaAtribuicoes();

    /**
     *
     * @param email
     * @return
     */
    public List<Evento> getListaEventosOrg(String email) {
        List leo = RegistoEventos.getListaEventosOrg(email);
        return leo;
    }
    
    public void setEvento(Evento Evento){
        this.Evento = Evento;
    }

    public List<AlgoritmoDeAtribuicao> getListaAlgoritmos() {
        List lalg = RegistoAlgoritmos.getListaAlgoritmos();
        return lalg;
    }

    public List<Atribuicao> setAlgoritmos(AlgoritmoDeAtribuicao Algoritmo) {
        List la = AlgoritmoDeAtribuicao.setAlgoritmo(Algoritmo);
        
        return la;
    }

    public void registaAtribuicao() {
        Evento.registaAtribuicao(Atribuicao);
        Evento.setEstado(EstadoEventoCandAtribuidas);
        Candidatura.setEstado(EstadoCandidaturaRegistada);
    }

}
