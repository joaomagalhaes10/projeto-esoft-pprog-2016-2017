package Controllers;

import Classes.AlgoritmoIdentificacao;
import Classes.CentroDeEventos;
import Classes.Congresso;
import Classes.Evento;
import Classes.ListaPeritosWorkshop;
import Classes.ListaTemas;
import Classes.ListaTemasWorkshop;
import Classes.ListaWorkshops;
import Classes.Perito;
import Classes.RegistoEventos;
import Classes.Tema;
import Classes.Workshop;
import java.util.List;

public class UC10_AlterarWorkshopController {

    CentroDeEventos CentroDeEventos;
    Congresso Congresso;
    Workshop Workshop;
    Tema Tema;
    AlgoritmoIdentificacao AlgoritmoIdentificação;
    Workshop Workshop_clone;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    ListaWorkshops ListaWorkshops = (ListaWorkshops) Congresso.getListaWorkshop();
    ListaTemas ListaTemas = (ListaTemas) CentroDeEventos.getListaTemas();

    public List<Evento> getListaCongressosOrg(String email) {
        List lcongo = RegistoEventos.getListaCongressosOrg(email);
        return lcongo;
    }

    public void setCongresso(Congresso Congresso) {
        this.Congresso = Congresso;
    }

    public List getListaWorkshops() {
        List lw = ListaWorkshops.getListaWorkshops();
        return lw;
    }

    public String setWorkshop(Workshop Workshop) {
        Workshop_clone = Workshop.criarCloneDeWorkshop(Workshop);
        ListaTemasWorkshop ltw_clone = new ListaTemasWorkshop();
        ListaPeritosWorkshop lpw_clone = new ListaPeritosWorkshop();
        
        String infow = ListaWorkshops.getInfoWorkshop(Workshop);
        
        return infow;
    }

    public List setDados(String codigo, String descricao) {
        Workshop_clone.setCodigo(codigo);
        Workshop_clone.setDescricao(descricao);
        
        ListaWorkshops.validaWorkshop(Workshop_clone);
        
        List lt = ListaTemas.getListaTemas();
        return lt;
    }

    public void setTema(Tema Tema) {
        Workshop_clone.setTema(Tema);
    }

    public Tema novoTema(String nome, String descricao) {
        Tema t = ListaTemas.setNovoTema();
        
        t.setNome(nome);
        t.setDescricao(descricao);
        
        this.Tema = t;
        
        return t;
    }

    public void registaTema() {
        ListaTemas.registaTema(Tema);
    }

    public List<Perito> setDados() {
        List<Perito> lps = AlgoritmoIdentificação.setDados();
        
        return lps;
    }

    public void setLista(List<Perito> lps) {
        Workshop_clone.setLista(lps);
    }

    public void registaAlteracoes() {
        ListaWorkshops.registaAlteracoes(Workshop, Workshop_clone);
    }

}
