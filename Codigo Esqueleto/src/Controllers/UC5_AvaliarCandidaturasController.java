package Controllers;

import Classes.Atribuicao;
import Classes.Candidatura;
import Classes.CentroDeEventos;
import Classes.Decisao;
import Classes.Evento;
import Classes.ListaAtribuicoes;
import Classes.ListaCandidaturas;
import Classes.RegistoEventos;
import Estados.EstadoCandidaturaAtribuida;
import Estados.EstadoEventoCandAvaliadas;
import java.util.List;

public class UC5_AvaliarCandidaturasController {

    CentroDeEventos CentroDeEventos;
    Evento Evento;
    Candidatura Candidatura;
    Decisao Decisao;
    EstadoEventoCandAvaliadas EstadoEventoCandAvaliadas;
    EstadoCandidaturaAtribuida EstadoCandidaturaAtribuida;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    ListaAtribuicoes ListaAtribuicoes = (ListaAtribuicoes) Evento.getListaAtribuicoes();
    ListaCandidaturas ListaCandidaturas = (ListaCandidaturas) Evento.getListaCandidaturas();

    /**
     *
     * @param email
     * @return
     */
    public List<Evento> getListaEventosFAE(String email) {
        List lefae = RegistoEventos.getListaEventoFAE(email);
        return lefae;
    }

    public List<Atribuicao> getListaAtribuicoes() {
        List la = ListaAtribuicoes.getListaAtribuicoes();
        return la;
    }

    public String getInfoCandidatura(Candidatura Candidatura) {
        String info = ListaCandidaturas.getInfoCandidatura(Candidatura);
        return info;
    }

    /**
     *
     * @param dec
     * @param txt
     */
    public void setDecisao(String dec, String txt) {
        Decisao = Candidatura.novaDecisao();
        Decisao.setDec(dec);
        Decisao.setTextoDesc(txt);
        Candidatura.validaDecisao(Decisao);
    }

    public void registaDecisao() {
        Candidatura.registaDecisao(Decisao);
        
        Evento.setEstado(EstadoEventoCandAvaliadas);
        Candidatura.setEstado(EstadoCandidaturaAtribuida);
    }

}
