package Controllers;

import Classes.CentroDeEventos;
import Classes.GestorDeEventos;
import Classes.ListaGestores;
import Classes.RegistoUtilizadores;
import Classes.Utilizador;
import java.util.List;

public class UC9_CriarGestorEventosController {

    CentroDeEventos CentroDeEventos;
    GestorDeEventos GestorDeEventos;
    Utilizador u;

    RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();
    ListaGestores ListaGestores = (ListaGestores) RegistoUtilizadores.getListaGestores();

    public List<Utilizador> getListaUtilizadoresConf() {
        List lu = RegistoUtilizadores.getListaUtilizadoresConf();
        return lu;
    }

    public void novoGestor(Utilizador Utilizador) {
        GestorDeEventos = ListaGestores.novoGestor(Utilizador);
    }

    public void registaGestor() {
        ListaGestores.registaGestor(GestorDeEventos);
    }

}
