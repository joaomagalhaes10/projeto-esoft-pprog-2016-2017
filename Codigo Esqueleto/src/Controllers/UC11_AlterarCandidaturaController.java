package Controllers;

import Classes.*;
import java.util.List;

public class UC11_AlterarCandidaturaController {
    
    CentroDeEventos CentroDeEventos;
    Evento Evento;
    Candidatura Candidatura_clone;
    Candidatura Candidatura;
    
    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    ListaCandidaturas ListaCandidaturas = (ListaCandidaturas) Evento.getListaCandidaturas();

    public List getListaCandidaturasRep(String email) {
        List lc = RegistoEventos.getListaCandidaturasRep(email);
        
        return lc;
    }

    public String setCandidaturaRep(Candidatura Candidatura) {
        this.Candidatura = Candidatura;
        Candidatura_clone = Candidatura.criarCloneCandidaturaRep(Candidatura);
        
        String infoc = ListaCandidaturas.getInfoCandidatura(Candidatura);
        
        return infoc;
    }

    public void setDados() {
        Candidatura_clone.setDados();
        
        ListaCandidaturas.validaCandidatura(Candidatura_clone);
    }

    public void registaAlteracoes() {
        ListaCandidaturas.registaAlteracoes(Candidatura, Candidatura_clone);
    }
    
}
