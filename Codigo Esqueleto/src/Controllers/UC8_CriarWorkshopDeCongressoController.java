package Controllers;

import Classes.*;
import java.util.List;

public class UC8_CriarWorkshopDeCongressoController {

    CentroDeEventos CentroDeEventos;
    Congresso Congresso;
    Workshop Workshop;
    Tema Tema;
    AlgoritmoIdentificacao AlgoritmoIdentificação;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    ListaWorkshops ListaWorkshops = (ListaWorkshops) Congresso.getListaWorkshop();
    ListaTemas ListaTemas = (ListaTemas) CentroDeEventos.getListaTemas();

    public List<Evento> getListaCongressosOrg(String email) {
        List lcongo = RegistoEventos.getListaCongressosOrg(email);
        return lcongo;
    }
    
    public void setCongresso(Congresso Congresso){
        this.Congresso = Congresso;
    }

    public void novoWorkshop() {
        Workshop = ListaWorkshops.novoWorkshop();
        ListaTemasWorkshop ltw = new ListaTemasWorkshop();
        ListaPeritosWorkshop lpw = new ListaPeritosWorkshop();
    }

    public List<Tema> setDados(String codigo, String descricao) {
        Workshop.getCodigo();
        Workshop.getDescricao();
                
        ListaWorkshops.validaWorkshop(Workshop);
        
        List lt = ListaTemas.getListaTemas();
        return lt;
    }
    
    public void setTema(Tema Tema){
        Workshop.setTema(Tema);
    }
    
    public Tema novoTema(String nome, String descricao){
        Tema t = ListaTemas.setNovoTema();
        
        t.setNome(nome);
        t.setDescricao(descricao);
        
        this.Tema = t;
        
        return t;
    }
    
    public void registaTema(){
        ListaTemas.registaTema(Tema);
    }
    
    public List<Perito> setDados(){
        
        List<Perito> lps = AlgoritmoIdentificação.setDados();
        
        return lps;
    }
    
    public void setLista(List<Perito> lps){
        Workshop.setLista(lps);
        
    }

    public void registaWorkshop() {
        ListaWorkshops.registaWorkshop(Workshop);
    }

}
