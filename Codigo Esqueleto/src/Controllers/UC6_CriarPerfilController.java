package Controllers;

import Classes.CentroDeEventos;
import Classes.RegistoUtilizadores;
import Classes.Utilizador;

public class UC6_CriarPerfilController {

    CentroDeEventos CentroDeEventos;
    Utilizador Utilizador;

    RegistoUtilizadores ru = CentroDeEventos.getRegistoUtilizadores();

    public void novoUtilizador() {
        Utilizador = ru.novoUtilizador();
    }

    /**
     *
     * @param nome
     * @param email
     * @param username
     * @param password
     * @param idioma
     * @param fusohorario
     */
    public void setDados(String nome, String email, String username, String password, String idioma, String fusohorario) {
        Utilizador.setNome(nome);
        Utilizador.setEmail(email);
        Utilizador.setUsername(username);
        Utilizador.setPassword(password);
        Utilizador.setIdioma(idioma);
        Utilizador.setFusohorario(fusohorario);

        ru.validaUtilizador(Utilizador);
    }

    public void registaUtilizador() {
        ru.registaUtilizador(Utilizador);
    }

}
