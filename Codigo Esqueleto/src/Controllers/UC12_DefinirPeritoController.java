package Controllers;

import Classes.*;
import Classes.Perito;

public class UC12_DefinirPeritoController {
    
    CentroDeEventos CentroDeEventos;
    Perito Perito;
    AreaPericia AreaPericia;
    
    ListaPeritos ListaPeritos = (ListaPeritos) CentroDeEventos.getListaPeritos();

    public void novoPerito() {
        this.Perito = ListaPeritos.novoPerito();
    }

    public void setDados(String nome, String areapericia) {
        Perito.setNome(nome);
        AreaPericia.setAreapericia(areapericia);
        
        Perito.valida();
    }

    public void registaPerito() {
        ListaPeritos.registaPerito(Perito);
    }
    
}
