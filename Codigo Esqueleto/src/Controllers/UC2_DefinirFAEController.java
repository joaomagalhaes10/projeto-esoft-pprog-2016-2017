package Controllers;

import Classes.CentroDeEventos;
import Classes.Evento;
import Classes.FAE;
import Classes.ListaFAE;
import Classes.RegistoEventos;
import Classes.RegistoUtilizadores;
import Classes.Utilizador;
import Estados.EstadoEventoCriado;
import java.util.List;

public class UC2_DefinirFAEController {

    CentroDeEventos CentroDeEventos;
    Evento Evento;
    Utilizador Utilizador;
    FAE Fae;
    EstadoEventoCriado EstadoEventoCriado;

    RegistoEventos RegistoEventos = CentroDeEventos.getRegistoEventos();
    RegistoUtilizadores RegistoUtilizadores = CentroDeEventos.getRegistoUtilizadores();
    ListaFAE ListaFAE = (ListaFAE) Evento.getListaFAE();


    public void setEvento(Evento Evento){
        this.Evento=Evento;
    }
    
    public List<Evento> getListaEventosOrg(String email) {
        List leo = RegistoEventos.getListaEventosOrg(email);

        return leo;
    }

    public List<Utilizador> getListaUtilizadoresConf() {              
        List lu = RegistoUtilizadores.getListaUtilizadoresConf();
        
        return lu;
    }

    public void addFAE(Utilizador u) {
        ListaFAE.addFAE(Utilizador, Evento);

    }

    public void registaFAE() {
        ListaFAE.registaFAE(Fae);
        Evento.setEstado(EstadoEventoCriado);
    }

}
