package UI;

import Classes.Evento;
import Controllers.UC3_CriarCandidaturaController;
import java.util.List;

public class UC3_CriarCandidaturaUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Evento Evento = new Evento();
        
        UC3_CriarCandidaturaController ctrl = new UC3_CriarCandidaturaController();
                
        List le = ctrl.getListaEventos();
        
        ctrl.setEvento(Evento);
        
        ctrl.novaCandidatura();
        
        ctrl.setDados();
        
        ctrl.registaCandidatura();
    }
    
}
