package UI;

import Classes.*;
import Controllers.UC1_CriarEventoController;

public class UC1_CriarEventoUI {

    /**
     * @param args the command line arguments
     */
       
    public static void main(String[] args) {
        
        Utilizador Utilizador = new Utilizador();
        
        UC1_CriarEventoController ctrl = new UC1_CriarEventoController();
        
        ctrl.novoEvento();
        
        ctrl.setDados("","","","","","","");
        
        ctrl.getListaUtilizadoresConf();
        
        String info = ctrl.getInfoUtilizador(Utilizador);
        
        ctrl.addOrganizador();
        
        ctrl.validaEvento();
        
        ctrl.registaEvento();   
    }
    
}
