package UI;

import Classes.Utilizador;
import Controllers.UC7_ConfirmarRegistoController;
import java.util.List;

public class UC7_ConfirmarRegistoUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Utilizador Utilizador = new Utilizador();
        
        UC7_ConfirmarRegistoController ctrl = new UC7_ConfirmarRegistoController();
        
        List lunc = ctrl.getListaUtilizadoresNaoConf();
        
        String info = ctrl.getInfoUtilizador(Utilizador);
        
        ctrl.confirmarRegistoUtilizador();
        
        
    }
    
}
