package UI;

import Classes.Congresso;
import Classes.Perito;
import Classes.Tema;
import Classes.Workshop;
import Controllers.UC10_AlterarWorkshopController;
import java.util.List;

public class UC10_AlterarWorkshopUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Congresso Congresso = new Congresso();
        Workshop Workshop = new Workshop();      
        Tema Tema = new Tema();
                
        UC10_AlterarWorkshopController ctrl = new UC10_AlterarWorkshopController();
        
        String email="";
        
        List lecongo = ctrl.getListaCongressosOrg(email);
        
        ctrl.setCongresso(Congresso);
        
        List lw = ctrl.getListaWorkshops();
        
        String infow = ctrl.setWorkshop(Workshop);
        
        List lt = ctrl.setDados("", "");
        
        ctrl.setTema(Tema);
        
        Tema t = ctrl.novoTema("", "");
        
        ctrl.registaTema();
        
        List<Perito> lps = ctrl.setDados();
        
        ctrl.setLista(lps);
        
        ctrl.registaAlteracoes();
        
    }


}
