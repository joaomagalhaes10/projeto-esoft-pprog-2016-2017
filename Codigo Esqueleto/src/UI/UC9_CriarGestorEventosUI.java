package UI;

import Classes.Utilizador;
import Controllers.UC9_CriarGestorEventosController;
import java.util.List;

public class UC9_CriarGestorEventosUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Utilizador Utilizador = new Utilizador();
        
        UC9_CriarGestorEventosController ctrl = new UC9_CriarGestorEventosController();
        
        List luc = ctrl.getListaUtilizadoresConf();
        
        ctrl.novoGestor(Utilizador);
        
        ctrl.registaGestor();
    }
    
}
