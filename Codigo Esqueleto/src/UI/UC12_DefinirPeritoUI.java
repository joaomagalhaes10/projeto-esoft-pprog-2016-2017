package UI;

import Controllers.UC12_DefinirPeritoController;

public class UC12_DefinirPeritoUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        UC12_DefinirPeritoController ctrl = new UC12_DefinirPeritoController();
        
        ctrl.novoPerito();
        
        ctrl.setDados("", "");
        
        ctrl.registaPerito();
        
    }
    
}
