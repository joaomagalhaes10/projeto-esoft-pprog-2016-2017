package UI;

import Classes.Congresso;
import Classes.Perito;
import Classes.Tema;
import Controllers.UC8_CriarWorkshopDeCongressoController;
import java.util.List;

public class UC8_CriarWorkshopDeCongressoUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Congresso Congresso = new Congresso();
        
        String email="";
        
        Tema Tema = new Tema();
        
        UC8_CriarWorkshopDeCongressoController ctrl = new UC8_CriarWorkshopDeCongressoController();

        List lcongo = ctrl.getListaCongressosOrg(email);

        ctrl.setCongresso(Congresso);
        
        ctrl.novoWorkshop();

        List lt = ctrl.setDados("", "");
        
        ctrl.setTema(Tema);
        
        Tema t = ctrl.novoTema("", "");
        
        ctrl.registaTema();
        
        List<Perito> lps = ctrl.setDados();
        
        ctrl.setLista(lps);

        ctrl.registaWorkshop();
        
    }
}
