package UI;

import Controllers.UC6_CriarPerfilController;

public class UC6_CriarPerfilUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        UC6_CriarPerfilController ctrl = new UC6_CriarPerfilController();
        
        ctrl.novoUtilizador();
        
        ctrl.setDados("", "", "", "", "", "");
        
        ctrl.registaUtilizador();
    }
    
}
